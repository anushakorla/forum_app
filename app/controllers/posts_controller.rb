class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :correct_user, only: [:edit, :update, :destroy]
 
  def index
    @posts = Post.all.order("created_at DESC")
    respond_to do |format|
      format.html 
      format.json { render json: @post }
      format.js
    end
  end

  
  def show
  end

  def new
    @post = current_user.posts.build
    respond_to do |format|
      format.html 
      format.json { render json: @post }
      format.js
    end
  end

 
  def edit
  end

  
  def create
    @post = current_user.posts.build(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
        format.js
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
      format.js  
    end
  end

   def correct_user
      if user_signed_in?
        post = Post.find(params[:id])
        @user = post.user_id
        @current_user = current_user.id
        if @user != @current_user
          redirect_to request.referrer || root_url, notice: 'Access Denied!'
        end
      end
    end

  private
  
    def set_post
      @post = Post.find(params[:id])
    end

   
    def post_params
      params.require(:post).permit(:title, :content)
    end
end
